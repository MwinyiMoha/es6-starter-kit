import {expect} from 'chai';
import fs from 'fs';
import jsdom from 'jsdom';

describe('First Test', ()=> {
    it('Should Pass', ()=> {
        expect(true).to.equal(true);
    });
});

describe('Index Page', ()=>{
    it('Should Say Hello ES6', (done)=> {
        const index = fs.readFileSync('./src/index.html', "utf-8");
        jsdom.env(index, function(err, window){
            const h1 = window.document.getElementsByTagName('h1')[0];
            expect(h1.innerHTML).to.equal('Hello ES6');
            done();
            window.close();
        });
    });
});
