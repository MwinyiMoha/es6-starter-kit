import numeral from 'numeral';

const val = numeral(1000).format('$0,0.00');
console.log(`Pay ${val} for this course`);
