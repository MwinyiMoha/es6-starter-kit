// NOTE:
// This file is not transpiled, so we should use CommonJS or ES5
// We use babel register so that code is transpiled first before tests run
// Final line disables webpack stuff that mocha does not understand

require('babel-register')();
require.extensions['.css'] = function(){};
